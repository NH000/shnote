# shnote

## Description
shnote is a simple TUI note-taking application written in Bash.
Its dependence on external programs is minimal, needing only coreutils and gettext to function properly.

#### What is missing
+ Support for RTL languages

## Requirements

#### For installation
+ make
+ sh
+ sed
+ msgfmt
+ coreutils

#### For running
+ bash
+ coreutils
+ gettext

## Installation
Here are the steps for the basic installation of this program. If you need to modify some parameters,
run `make help` to get the list of supported options.

1. Go into the project's root directory.
1. Run `make install`. This will install the script and all its assets (only translation files for now).

## Uninstallation
If you want to uninstall the program, make sure that make variables are set to the identical values
used during the installation of the program, and follow this algorithm.

1. Go into the project's root directory.
1. Run `make uninstall`. This will uninstall the program and its installed assets.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

Also, it would be good to translate the manual page; you will find it in `man` project subdirectory.
