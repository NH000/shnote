# INSTALL COMMANDS
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# VARIABLES
PROG := shnote
INSTALLDIR := /usr/bin
LOCALEDIR := /usr/share/locale

# FUNCTIONS

# Generate MO file from PO file into $(LOCALEDIR) given language code as $(1).
generate_mo = mkdir -p '$(LOCALEDIR)/$(1)/LC_MESSAGES' && msgfmt -o '$(LOCALEDIR)/$(1)/LC_MESSAGES/$(PROG).mo' 'po/$(1).po'

# Delete MO file from $(LOCALEDIR) given language code as $(1).
# Then delete all of its parent directories in bottom-to-top order if they are empty.
uninstall_mo = if test -d '$(LOCALEDIR)/$(1)/LC_MESSAGES'; then rm -f '$(LOCALEDIR)/$(1)/LC_MESSAGES/$(PROG).mo' && rmdir -p --ignore-fail-on-non-empty '$(LOCALEDIR)/$(1)/LC_MESSAGES'; fi

# RULES

.PHONY: help install uninstall

help:
	$(info VARIABLES)
	$(info ================================================================================)
	$(info PROG:       Name of the program as installed.)
	$(info INSTALLDIR: Installation directory.)
	$(info LOCALEDIR:  Locale installation directory.)
	$(info )
	$(info RULES)
	$(info ================================================================================)
	$(info help:      Display this help menu.)
	$(info install:   Install the program and its assets.)
	$(info uninstall: Uninstall the program and its assets.)

install: shnote.sh po/*.po
	$(INSTALL_PROGRAM) -D '$<' '$(INSTALLDIR)/$(PROG)'
	@sed -i "20s/$$/'$(subst /,\/,$(LOCALEDIR))'/;21s/$$/'$(PROG)'/" '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call generate_mo,$(patsubst po/%.po,%,$(po)));)

uninstall:
	rm -f '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call uninstall_mo,$(patsubst po/%.po,%,$(po)));)
