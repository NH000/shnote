#!/bin/env bash

# Copyright (C) 2019-2021 Nikola Hadžić
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Translation variables.
# Set during installation.
export TEXTDOMAINDIR=
export TEXTDOMAIN=

# Translation functions.
source gettext.sh

# Trims all leading and trailing whitespace from the string.
trim_string()
{
    : "${1#"${1%%[![:space:]]*}"}"
    : "${_%"${_##*[![:space:]]}"}"
    echo -en "$_"
}

# Makes string eligible for displaying: converts tabs to spaces and cuts out part of the string that cannot fit.
show_string()
{
    : "$(echo "$1" | expand -t $opt_tabs)"

    if [[ ${#_} -gt ${term_size[1]} ]]; then
        echo -en "${_::$((${term_size[1]}-1))}\u2026"
    else
        echo -en "$_"
    fi
}

# Preperes terminal for the TUI.
setup_term()
{
    printf "\033[0m\033[${opt_foreground};${opt_background}m\033[2J\033[?25l"
    stty -echo

    # 1. Turn off all attributes.
    # 2. Set foreground and background colors.
    # 3. Clear the screen.
    # 4. Hide the cursor.
    # 5. Disable echoing.
}

# Resets terminal to usable state.
reset_term()
{
    printf "\033[0m\033[2J\033[H\033[?25h"
    stty echo

    # 1. Turn off all attributes.
    # 2. Clear the screen.
    # 3. Position cursor at top-left corner.
    # 4. Unhide the cursor.
    # 5. Enable echoing.
}

# Gets terminal size.
get_term_size()
{
    declare -gai term_size=($(stty size))
}

# Returns foreground color code given color name.
get_foreground()
{
    case "$1" in
        "default")
            echo -n 39
            ;;
        "black")
            echo -n 30
            ;;
        "red")
            echo -n 31
            ;;
        "green")
            echo -n 32
            ;;
        "yellow")
            echo -n 33
            ;;
        "blue")
            echo -n 34
            ;;
        "magenta")
            echo -n 35
            ;;
        "cyan")
            echo -n 36
            ;;
        "white")
            echo -n 37
            ;;
        "lightblack")
            echo -n 90
            ;;
        "lightred")
            echo -n 91
            ;;
        "lightgreen")
            echo -n 92
            ;;
        "lightyellow")
            echo -n 93
            ;;
        "lightblue")
            echo -n 94
            ;;
        "lightmagenta")
            echo -n 95
            ;;
        "lightcyan")
            echo -n 96
            ;;
        "lightwhite")
            echo -n 97
            ;;
        *)
            local -r prog_name="$0"
            local -r color="$1"
            echo "$(eval_gettext "\$prog_name: no such foreground color -- '\$color'")" 1>&2
            exit 1
            ;;
    esac
}

# Returns background color code given color name.
get_background()
{
    case "$1" in
        "default")
            echo -n 49
            ;;
        "black")
            echo -n 40
            ;;
        "red")
            echo -n 41
            ;;
        "green")
            echo -n 42
            ;;
        "yellow")
            echo -n 43
            ;;
        "blue")
            echo -n 44
            ;;
        "magenta")
            echo -n 45
            ;;
        "cyan")
            echo -n 46
            ;;
        "white")
            echo -n 47
            ;;
        "lightblack")
            echo -n 100
            ;;
        "lightred")
            echo -n 101
            ;;
        "lightgreen")
            echo -n 102
            ;;
        "lightyellow")
            echo -n 103
            ;;
        "lightblue")
            echo -n 104
            ;;
        "lightmagenta")
            echo -n 105
            ;;
        "lightcyan")
            echo -n 106
            ;;
        "lightwhite")
            echo -n 107
            ;;
        *)
            local -r prog_name="$0"
            local -r color="$1"
            echo "$(eval_gettext "\$prog_name: no such background color -- '\$color'")" 1>&2
            exit 1
            ;;
    esac
}

# Splits given string into lines whose length is
# specified in the second argument.
split_string()
{
    declare -ga split_lines=()
    local -i i
    for ((i=0;i<${#1};i+=$2)); do
        split_lines+=("${1:$i:$2}")
    done
}

# Displays help menu.
help_menu()
{
    echo "shnote - $(gettext "CLI note taking app")"
    echo "$(gettext "Version"): 1.0"
    echo "$(gettext "License"): GPL3"
    echo
    echo "$(gettext "USAGE"):"
    echo "$0 [-h] [-b $(gettext "COLOR")] [-f $(gettext "COLOR")] [-t $(gettext "FILE")] [--tabs $(gettext "NUMBER")]"
    echo
    echo "$(gettext "OPTIONS"):"
    echo "-h, --help"
    echo -e "\t$(gettext "display help menu and exit")"
    echo "-b $(gettext "COLOR"), --backgrond $(gettext "COLOR")"
    echo -e "\t$(gettext "set background color to COLOR")"
    echo "-f $(gettext "COLOR"), --foreground $(gettext "COLOR")"
    echo -e "\t$(gettext "set foreground color to COLOR")"
    echo "-t $(gettext "FILE"), --todo $(gettext "FILE")"
    echo -e "\t$(gettext "set todo file to FILE")"
    echo "--tabs $(gettext "NUMBER")"
    echo -e "\t$(gettext "number of spaces tabulator is displayed as (tabulators are always saved)")"
}

# Parses command-line arguments and sets option variables accordingly.
# Pass entire command-line to this function.
parse_args()
{
    # Parse command-line arguments.
    while [[ $# -gt 0 ]]; do
        case "$1" in
            "-h"|"--help")
                help_menu
                exit 0
                ;;
            "-f"|"--foreground")
                if [[ $(($# - 1)) -eq 0 ]]; then
                    local -r prog_name="$0"
                    local -r option="$1"
                    echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                    exit 1
                fi

                opt_foreground=$(get_foreground "$2")
                shift 2
                ;;
            "-b"|"--background")
                if [[ $(($# - 1)) -eq 0 ]]; then
                    local -r prog_name="$0"
                    local -r option="$1"
                    echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                    exit 1
                fi

                opt_background=$(get_background "$2")
                shift 2
                ;;
            "-t"|"--todo")
                if [[ $(($# - 1)) -eq 0 ]]; then
                    local -r prog_name="$0"
                    local -r option="$1"
                    echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                    exit 1
                fi

                opt_todo="$2"
                shift 2
                ;;
            "--tabs")
                if [[ $(($# - 1)) -eq 0 ]]; then
                    local -r prog_name="$0"
                    local -r option="$1"
                    echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                    exit 1
                fi

                opt_tabs="$2"
                shift 2
                ;;
            *)
                local -r prog_name="$0"
                local -r option="$1"
                echo "$(eval_gettext "\$prog_name: unrecognized option -- '\$option'")" 1>&2
                exit 1
        esac
    done
}

# Reads todo file and populates todo item array line by line (empty lines are ignored).
# If reading is unsuccusful error is ignored.
todo_read()
{
    ! 2>/dev/null IFS=$'\n' read -d '' -r -a todo_items < "$opt_todo"

    # Trim leading and trailing whitespace from notes.
    local -i i
    for ((i=0;i<${#todo_items[@]};++i)); do
        todo_items[$i]="$(trim_string "${todo_items[$i]}")"
    done
}

# Writes todo item list to todo file. Each element is written as a line.
# If writing fails user will be asked to enter another filename.
todo_write()
{
    # First try saving to the specified todo file.
    local save_file="$opt_todo"

    # While todo items are not saved...
    while :; do
        # Try creating both the directory and the file
        # (if they do not exist).
        ! mkdir -p "$(dirname "$save_file")" 2>/dev/null
        ! 2>/dev/null > "$save_file"

        # If succussful, save todo array to the file and break.
        if [[ ${PIPESTATUS[0]} -eq 0 ]]; then
            for line in "${todo_items[@]}"; do
                echo "$line" >> "$save_file"
            done
            break
        fi

        # If not succussful, ask user for another filename and try again.
        echo "$(gettext "Saving to file failed!")" 1>&2
        stty echo
        printf "\033[?25h"
        read -e -p "$(gettext "Save to another file (empty to cancel): ")" -r save_file
        printf "\033[?25l"
        stty -echo

        # If user does not specify another filename, exit without saving.
        if [[ -z "$save_file" ]]; then
            exit 3
        fi
    done
}

# Draws program logo.
draw_logo()
{
    printf "\033[H"                                 # Move cursor to the top-left corner.
    printf "\033[7m$(show_string "shnote")\033[27m" # Write program name with reverse foreground and background colors.
}

# Draws item list.
# If there are no items, notification is displayed.
draw_list()
{
    local -ir page_height=$((${term_size[0]}-2))

    if [[ $page_height -le 0 ]]; then
        return
    fi

    # Starting after the title, write contents of each item on the screen as a list item.
    if [[ ${#todo_items[@]} -ne 0 ]]; then
        local -ir start=$(($position/$page_height*$page_height))    # Determine todo item index that should appear at the top.

        local -i i
        for ((i=$start;$(($i-$start)) < $page_height && i < ${#todo_items[@]};++i)); do
            printf "\033[$(($i-$start+2))H\033[K"   # Move to appropriate row and clean it.

            if [[ $i -eq $position ]]; then
                printf "\033[7m$(show_string "$(($selected+1)). ${todo_items[$selected]}")\033[27m" # Reverse foreground and background colors for the selected item.
            elif [[ $i -gt $position && $i -le $selected ]]; then
                printf "$(show_string "$i. ${todo_items[$(($i-1))]}")"
            elif [[ $i -ge $selected && $i -lt $position ]]; then
                printf "$(show_string "$(($i+2)). ${todo_items[$(($i+1))]}")"
            else
                printf "$(show_string "$(($i+1)). ${todo_items[$i]}")"
            fi
        done
    else    # Display notification message.
        printf "\033[2H"
        printf "\033[3m$(show_string "$(gettext "You have no notes.")")\033[0m"
    fi
}

# Draws control menu depending on the current mode.
draw_controls()
{
    # Go to the last row and clean it.
    printf "\033[${term_size[0]}H\033[K"

    # Draw control menu in accordance to the current mode.
    # Menu is also drawn with reverse background and foreground colors.
    if [[ "$mode" == "normal" ]]; then
        if [[ ${#todo_items[@]} -eq 0 ]]; then
            printf "\033[7m$(show_string "$(gettext "[a]dd [q]uit")")\033[27m"
        else
            printf "\033[7m$(show_string "$(gettext "[a]dd [v]iew [e]dit [m]ove [d]elete [q]uit")")\033[27m"
        fi
    elif [[ "$mode" == "view" ]]; then
        printf "\033[7m$(show_string "$(gettext "[b]ack")")\033[27m"
    elif [[ "$mode" == "move" ]]; then
        printf "\033[7m$(show_string "$(gettext "[p]lace [c]ancel")")\033[27m"
    fi
}

# Draws string array over the entire screen, as calculated
# by split_string().
# Lines previous to the line containing the starting
# character are not printed.
draw_full_note()
{
    local -i current_line=1

    local -i i
    for ((i=0;i < ${#split_lines[@]} && $current_line < ${term_size[0]};++i)); do
        if [[ $start_char -lt $((($i+1)*${term_size[1]})) ]]; then
            printf "\033[${current_line}H${split_lines[$i]}"
            ((current_line++))
        fi
    done
}

# Performs action when user presses a key. Action to be
# performed is also determined by the current mode.
key_pressed()
{
    local -i i
    if [[ "$mode" == "normal" ]]; then  # NORMAL MODE
        if [[ "$1" == "$(gettext "a")" ]]; then # Add new todo item to the end of the list.
            # Prompt user to enter the new note.
            printf "\033[${term_size[0]}H\033[K\033[?25h"
            stty echo
            read -e -p "$(gettext "Enter new note (empty to cancel): ")" -r 2>&1
            stty -echo
            printf "\033[?25l"

            # Redraw logo that disappeared when user pressed enter.
            printf "\033[T"
            draw_logo

            # Trim all leading and trailing whitespace from the response.
            REPLY="$(trim_string "$REPLY")"

            # If user entered note, add it to the todo item array and update list on the screen.
            # Let newly added note become selected note.
            if [[ -n "$REPLY" ]]; then
                # Push new note to the end of todo item array.
                todo_items+=("$REPLY")

                # Save old pointers.
                local -ir old_selected=$selected
                local -ir old_position=$position

                # Update `selected` and `position` pointers to point at the new note.
                selected=$((${#todo_items[@]}-1))
                position=$selected

                # If new note is on the current page, just redraw current note as non-selected
                # and draw last (that is, new) as selected.
                local -ir page_height=$((${term_size[0]}-2))
                if [[ $(($old_position/$page_height)) -eq $(($position/$page_height)) ]]; then
                    # 1. Delete the current line, and
                    # 2. Redraw it as non-selected, but only if this is not the first item.
                    printf "\033[$((($old_position%$page_height)+2))H\033[K"
                    if [[ ${#todo_items[@]} -ne 1 ]]; then
                        printf "$(show_string "$(($old_position+1)). ${todo_items[$old_selected]}")"
                    fi

                    # Jump to the last line and draw it as selected.
                    printf "\033[$((($position%$page_height)+2))H\033[7m$(show_string "$(($position+1)). ${todo_items[$selected]}")\033[27m"
                else    # Otherwise, fully redraw the list since the entire page needs to be changed.
                    # Erase list.
                    for ((i=2;i<${term_size[0]};++i)); do
                        printf "\033[${i}H\033[K"
                    done

                    # Redraw list.
                    draw_list
                fi
            fi

            # Redraw control menu.
            draw_controls
        elif [[ "$1" == "$(gettext "v")" || -z "$1" ]] && [[ ${#todo_items[@]} -ne 0 ]]; then   # Display currently selected note in fullscreen.
            mode="view"

            # Split currently selected note into slices each of length of the width of the terminal,
            # and initialize the starting character to see.
            split_string "$(echo -n "${todo_items[$selected]}" | expand -t $opt_tabs)" ${term_size[1]}
            declare -ig start_char=0

            # Draw split note in fullscreen, and update control menu.
            printf "\033[2J"
            draw_full_note
            draw_controls
        elif [[ "$1" == "$(gettext "e")" && ${#todo_items[@]} -ne 0 ]]; then    # Edit currently selected entry.
            # Display prompt to user that lets him modify the note's text.
            # Current note's text value is supplied as the default.
            printf "\033[${term_size[0]}H\033[K\033[?25h"
            stty echo
            read -e -i "${todo_items[$selected]}" -p "$(gettext "Edit note (empty to cancel): ")" -r 2>&1
            stty -echo
            printf "\033[?25l"

            # Redraw logo that disappeared when user pressed enter.
            printf "\033[T"
            draw_logo

            # Trim all leading and trailing whitespace.
            REPLY="$(trim_string "$REPLY")"

            # Update name of the current note if new name is not empty.
            if [[ -n "$REPLY" ]]; then
                todo_items[$selected]="$REPLY"

                local -ir page_height=$((${term_size[0]}-2))

                # Update currently selected note's text on screen.
                printf "\033[$((($position%$page_height)+2))H\033[K\033[7m$(show_string "$(($position+1)). ${todo_items[$selected]}")\033[27m"
            fi

            # Redraw control menu.
            draw_controls
        elif [[ "$1" == "$(gettext "m")" && ${#todo_items[@]} -ne 0 ]]; then    # Enter mode that allows user to change order of items in the list.
            mode="move"
            draw_controls
        elif [[ "$1" == "$(gettext "d")" && ${#todo_items[@]} -ne 0 ]]; then    # Delete currently selected note, or all notes.
            # Prompt user whether he wants to delete the current note, all notes, or neither.
            printf "\033[${term_size[0]}H\033[K\033[?25h"
            stty echo
            read -e -p "$(gettext "Are you sure that you want to delete this note? (yes/No/all) ")" -r 2>&1
            stty -echo
            printf "\033[?25l"

            # Redraw logo that disappeared when user pressed enter.
            printf "\033[T"
            draw_logo

            shopt -s nocasematch

            local -ir page_height=$((${term_size[0]}-2))

            if [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "yes")"[[:blank:]]*$ ]]; then # Deletes only currently selected note.
                # Modify todo item array such that it does contain currently selected item anymore.
                # Also clear line that displays currently selected note.
                todo_items=("${todo_items[@]::$selected}" "${todo_items[@]:$(($selected+1))}")
                printf "\033[$((($selected%$page_height)+2))H\033[K"

                # If there are no more elements, show message that there are no more elements.
                if [[ ${#todo_items[@]} -eq 0 ]]; then
                    draw_list
                else
                    # If this was the last element, let current element become
                    # second to last element. Otherwise, current element will
                    # just be the next element.
                    if [[ $selected -eq ${#todo_items[@]} ]]; then
                        selected=$((${#todo_items[@]}-1))
                        position=$selected
                        printf "\033[F\033[K"
                    fi

                    # Draw the new current element as selected.
                    printf "\033[7m$(show_string "$(($selected+1)). ${todo_items[$selected]}")\033[27m"

                    # Starting from the new current element, redraw all subsequent elements, so that their
                    # indexes are valid.
                    for ((i=$(($selected+1));$(($i/$page_height*$page_height))<$page_height && i<${#todo_items[@]};++i)); do
                        printf "\033[E\033[K$(show_string "$(($i+1)). ${todo_items[$i]}")"
                    done

                    # If this page is not completely full, remove last list item, as the entire list moved up.
                    if [[ $((${#todo_items[@]} - $selected/$page_height*$page_height)) -lt $page_height ]]; then
                        printf "\033[E\033[K"
                    fi
                fi
            elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "all")"[[:blank:]]*$ ]]; then   # Delete the entire todo item array and clear the entire list on the screen.
                # Empty the todo item array and reset its pointers.
                todo_items=()
                position=0
                selected=0

                # Clear the entire list...
                for ((i=0;i<$page_height;++i)); do
                    printf "\033[$(($i+2))H\033[K"
                done

                # ...then redraw it, which means that only notification message will be displayed.
                draw_list
            fi

            # Redraw control menu.
            draw_controls

            shopt -u nocasematch
        elif [[ "$1" == "$(gettext "q")" ]]; then   # Quit the program.
            running=0
        elif [[ "$1" == "<" && $position -gt 0 ]]; then # Move up in the list.
            # Move current note pointers and determine pages on which the current and original position
            # of the selected note is.
            local -ir page_height=$((${term_size[0]}-2))
            local -ir old_selected=$selected
            local -ir old_position=$position
            position=$(($position-1))
            selected=$(($selected-1))
            local -ir old_page=$(($old_position/$page_height))
            local -ir new_page=$(($position/$page_height))

            # Depending on whether they are on the same page or not, redraw list accordingly.

            # If they are on the same page, redraw old entry as not selected and new as selected.
            if [[ $old_page -eq $new_page ]]; then
                printf "\033[$((($old_position%$page_height)+2))H\033[K$(show_string "$(($old_position+1)). ${todo_items[$old_selected]}")"
                printf "\033[F\033[K\033[7m$(show_string "$(($position+1)). ${todo_items[$selected]}")\033[27m"
            else    # Otherwise, redraw the entire list.
                # Erase list.
                for ((i=2;i<${term_size[0]};++i)); do
                    printf "\033[${i}H\033[K"
                done

                # Redraw list.
                draw_list
            fi
        elif [[ "$1" == ">" && $position -lt $((${#todo_items[@]}-1)) ]]; then  # Move down in the list.
            # Move current note pointers and determine pages on which the current and original position
            # of the selected note is.
            local -ir page_height=$((${term_size[0]}-2))
            local -ir old_selected=$selected
            local -ir old_position=$position
            position=$(($position+1))
            selected=$(($selected+1))
            local -ir old_page=$(($old_position/$page_height))
            local -ir new_page=$(($position/$page_height))

            # Depending on whether they are on the same page or not, redraw list accordingly.

            # If they are on the same page, redraw old entry as not selected and new as selected.
            if [[ $old_page -eq $new_page ]]; then
                printf "\033[$((($old_position%$page_height)+2))H\033[K$(show_string "$(($old_position+1)). ${todo_items[$old_selected]}")"
                printf "\033[E\033[K\033[7m$(show_string "$(($position+1)). ${todo_items[$selected]}")\033[27m"
            else    # Otherwise, redraw the entire list.
                # Erase list.
                for ((i=2;i<${term_size[0]};++i)); do
                    printf "\033[${i}H\033[K"
                done

                # Redraw list.
                draw_list
            fi
        fi
    elif [[ "$mode" == "move" ]]; then  # MOVE MODE
        if [[ "$1" == "$(gettext "p")" ]]; then # Place currently selected item into the current position and return to normal state.
            mode="normal"

            local -ir page_height=$((${term_size[0]}-2))

            # Redraw current line with index of the current position but with value of the selected note.
            printf "\033[$((($position%$page_height)+2))H\033[K\033[7m$(show_string "$(($position+1)). ${todo_items[$selected]}")\033[27m"

            # Redraw all other items so that their indexes correctly match new list state.

            # If selected item has been moved downwards, all indexes between its original and new position
            # must be incremented.
            if [[ $selected -lt $position ]]; then
                # If new and original positions of the selected item are on the same page,
                # redraw, starting from the previous line, each item starting from the item
                # that previously occupied new selected position, until original position
                # of the selected item is reached, but with the indexes of the actual line
                # positions.
                if [[ $(($selected/$page_height)) -eq $(($position/$page_height)) ]]; then
                    for ((i=$(($position-1));i>=$selected;--i)); do
                        printf "\033[F\033[K$(show_string "$(($i+1)). ${todo_items[$(($i+1))]}")"
                    done
                else    # Otherwise, do the same until top of the list is reached.
                    for ((i=$(($position-1));i>=$(($position/$page_height*$page_height));--i)); do
                        printf "\033[F\033[K$(show_string "$(($i+1)). ${todo_items[$(($i+1))]}")"
                    done
                fi

                # Properly modify the todo item array:
                #   1. First cut until the selected item (exclusive).
                #   2. Then from the item following the selected item (exclusive) until the item whose position is now being occupied by the selected item (inclusive).
                #   3. Then insert selected item.
                #   4. Then from the item whose position is now being occupied by the selected item (exclusive) to the end of the array.
                todo_items=("${todo_items[@]::$selected}" "${todo_items[@]:$(($selected+1)):$(($position-$selected))}" "${todo_items[$selected]}" "${todo_items[@]:$(($position+1))}")
            elif [[ $position -lt $selected ]]; then    # If selected item has been moved upward, all indexes between its original and new position must be decremented.
                # If new and original positions of the selected item are on the same page,
                # redraw, starting from the next line, each item starting from the item
                # that previously occupied new selected position, until original position
                # of the selected item is reached, but with the indexes of the actual line
                # positions.
                if [[ $(($selected/$page_height)) -eq $(($position/$page_height)) ]]; then
                    for ((i=$(($position+1));i<=$selected;++i)); do
                        printf "\033[E\033[K$(show_string "$(($i+1)). ${todo_items[$(($i-1))]}")"
                    done
                else    # Otherwise, do the same until bottom of the list is reached.
                    for ((i=$(($position+1));i<=$((($position/$page_height+1)*$page_height-1));++i)); do
                        printf "\033[E\033[K$(show_string "$(($i+1)). ${todo_items[$(($i-1))]}")"
                    done
                fi

                # Properly modify the todo item array:
                #   1. First cut until the item whose position is now being occupied by the selected item (exclusive).
                #   2. Then insert selected item.
                #   3. Then from the item whose position is now being occupied by the selected item (inclusive) to the selected item (exclusive).
                #   4. Then from the selected item (exclusive) to the end of the array.
                todo_items=("${todo_items[@]::$position}" "${todo_items[$selected]}" "${todo_items[@]:$position:$(($selected-$position))}" "${todo_items[@]:$(($selected+1))}")
            fi

            # Let selected be identical to position, since selected item has been moved to new position.
            selected=$position

            # Redraw control menu (since mode has been changed).
            draw_controls
        elif [[ "$1" == "$(gettext "c")" ]]; then   # Cancel the move and return to normal state. Selected item is returned to its original position and list is redrawn to display that.
            mode="normal"

            local -ir page_height=$((${term_size[0]}-2))

            # If the selected note has not moved from its original page, redraw it and all items it has stepped over,
            # such that they are all in their proper places.
            if [[ $(($selected/$page_height)) -eq $(($position/$page_height)) ]]; then
                # Jump to the original position of the currently selected note, and draw currently selected note over it.
                printf "\033[$((($selected%$page_height)+2))H\033[K\033[7m$(show_string "$(($selected+1)). ${todo_items[$selected]}")\033[27m"

                if [[ $selected -lt $position ]]; then
                    # If the note has moved downwards, starting from the original position and
                    # going downwards, redraw all notes until position previously occupied by
                    # selected note is reached.
                    for ((i=$(($selected+1));i<=$position;++i)); do
                        printf "\033[E\033[K$(show_string "$(($i+1)). ${todo_items[$i]}")"
                    done
                elif [[ $position -lt $selected ]]; then
                    # If the note has moved upwards, starting from the original position and
                    # going upwards, redraw all notes until position previously occupied by
                    # selected note is reached.
                    for ((i=$(($selected-1));i>=$position;--i)); do
                        printf "\033[F\033[K$(show_string "$(($i+1)). ${todo_items[$i]}")"
                    done
                fi

                # Lastly, let position of the note be again the original position.
                position=$selected
            else    # Else, adjust todo item array pointers and redraw the list.
                position=$selected

                for ((i=2;i<${term_size[0]};++i)); do
                    printf "\033[${i}H\033[K"
                done

                draw_list
            fi

            # Redraw control menu (since mode has been changed).
            draw_controls
        elif [[ "$1" == "<" && $position -gt 0 ]]; then # Move currently selected item up in the list.
            # Move current note position pointer up and determine pages on which the current and previous position
            # of the selected note is.
            local -ir page_height=$((${term_size[0]}-2))
            local -ir old_position=$position
            position=$(($position-1))
            local -ir old_page=$(($old_position/$page_height))
            local -ir new_page=$(($position/$page_height))

            # If current and previous positions of the selected note are on the same page,
            # exchange contents of those two lines (thus it looks like the note has moved up).
            if [[ $old_page -eq $new_page ]]; then
                # Delete contents of the previous line.
                printf "\033[$((($old_position%$page_height)+2))H\033[K"

                # If compared to the original position, the note is still
                # below or equal to it, old line must take on its corresponding
                # value from the array.
                if [[ $selected -ge $old_position ]]; then
                    printf "$(show_string "$(($position+1)). ${todo_items[$position]}")"
                else    # Otherwise, the line should contain value of the item below its corresponding item.
                    printf "$(show_string "$(($old_position+1)). ${todo_items[$old_position]}")"
                fi

                # Go to the new position (which is always up), clean it and draw selected item.
                printf "\033[F\033[K\033[7m$(show_string "$(($selected+1)). ${todo_items[$selected]}")\033[27m"
            else    # Otherwise, the entire list should be redrawn.
                for ((i=2;i<${term_size[0]};++i)); do
                    printf "\033[${i}H\033[K"
                done

                draw_list
            fi
        elif [[ "$1" == ">" && $position -lt $((${#todo_items[@]}-1)) ]]; then  # Move currently selected item down in the list.
            # Move current note position pointer down and determine pages on which the current and previous position
            # of the selected note is.
            local -ir page_height=$((${term_size[0]}-2))
            local -ir old_position=$position
            position=$(($position+1))
            local -ir old_page=$(($old_position/$page_height))
            local -ir new_page=$(($position/$page_height))

            # If current and previous positions of the selected note are on the same page,
            # exchange contents of those two lines (thus it looks like the note has moved down).
            if [[ $old_page -eq $new_page ]]; then
                # Delete contents of the previous line.
                printf "\033[$((($old_position%$page_height)+2))H\033[K"

                # If compared to the original position, the note is still
                # above or equal to it, old line must take on its corresponding
                # value from the array.
                if [[ $selected -le $old_position ]]; then
                    printf "$(show_string "$(($position+1)). ${todo_items[$position]}")"
                else    # Otherwise, the line should contain value of the item after its corresponding item.
                    printf "$(show_string "$(($old_position+1)). ${todo_items[$old_position]}")"
                fi

                # Go to the new position (which is always down), clean it and draw selected item.
                printf "\033[E\033[K\033[7m$(show_string "$(($selected+1)). ${todo_items[$selected]}")\033[27m"
            else    # Otherwise, the entire list should be redrawn.
                for ((i=2;i<${term_size[0]};++i)); do
                    printf "\033[${i}H\033[K"
                done

                draw_list
            fi
        fi
    elif [[ "$mode" == "view" ]]; then  # VIEW MODE
        if [[ "$1" == "$(gettext "b")" ]]; then # Exit fullscreen view.
            mode="normal"

            # Clean the entire screen and redraw it for normal mode.
            printf "\033[2J"
            draw_logo
            draw_list
            draw_controls
        elif [[ "$1" == "<" && $start_char -ne 0 ]]; then   # Scroll the preview up.
            # Delete the current preview, move start character backwards by one terminal line, and redraw the preview.
            printf "\033[$((${term_size[0]}-1));${term_size[1]}H\033[1J"
            start_char=$(($start_char-${term_size[1]}))
            draw_full_note
        elif [[ "$1" == ">" && $(($start_char+${term_size[1]})) -lt ${#todo_items[$selected]} ]]; then  # Scroll the preview down.
            # Delete the current preview, move start character forwards by one terminal line, and redraw the preview.
            printf "\033[$((${term_size[0]}-1));${term_size[1]}H\033[1J"
            start_char=$(($start_char+${term_size[1]}))
            draw_full_note
        fi
    fi
}

# Prepare shell environment:
#   1. Exit on error.
#   2. Let exit status of a pipeline be exit status of the last failed command.
#   3. Using unset variables results in error.
#   4. No pathname expansion in strings.
#   5. No updating window size after every command.
#   6. Matching is case sensitive.
#   7. Readline does not wrap.
#   8. Readline does not auto-complete on <Tab>.
set -o errexit -o pipefail -o nounset -o noglob
shopt -u checkwinsize nocasematch
bind "set horizontal-scroll-mode On" 2>/dev/null
bind "set disable-completion On"

# Declare command-line options.
declare -i opt_foreground=39                                    # Foreground color code.
declare -i opt_background=49                                    # Background color code.
declare opt_todo="${XDG_DATA_HOME:-~/.local/share}/shnote.txt"  # Todo filename.
declare opt_tabs=4                                              # Number of spaces to display tab as.

declare -a todo_items=()    # Todo item array.

# Pointers of currently selected item.
# They are interchangably in all modes except in "move" mode
# (nonetheless, for readability, in non-"move" modes `position`
# will be used to calculate position on the screen and
# `selected` as todo item array index).
declare -i selected=0   # Currently selected item as in the todo item array index.
declare -i position=0   # Todo index that would currently selected item occupy if it were positioned there.

declare mode="normal"   # Mode the program is running in.
declare -i running=1    # While true main loop is run.

# Parse command-line arguments.
parse_args "$@"

# Read todo file into the todo item array.
todo_read

# Trap signals.
#   1. Redraw screen appropriately when window size is changed.
#   2. Exit when SIGTERM is received.
#   3. Exit when SIGINT is set (with an error code).
#   4. Ignore suspension (SIGTSTP).
#   5. Put terminal into usable state when exiting.
trap 'printf "\033[2J"; get_term_size; if [[ "$mode" == "normal" || "$mode" == "move" ]]; then draw_logo; draw_list; draw_controls; elif [[ "$mode" == "view" ]]; then split_string "$(echo -n "${todo_items[$selected]}" | expand -t $opt_tabs)" ${term_size[1]}; start_char=$(($start_char/${term_size[1]}*${term_size[1]})); draw_full_note; draw_controls; fi' SIGWINCH # Redraw when screen size changes.
trap 'exit 0' SIGTERM
trap 'exit 2' SIGINT
trap '' SIGTSTP
trap 'reset_term' EXIT

# Initialize terminal state.
setup_term

# Get terminal size. Needed for drawing.
get_term_size

# Draw screen:
#   1. Program logo.
#   2. List that contains items from todo item array (as many as can fit).
#   3. Control menu.
draw_logo
draw_list
draw_controls

# Main loop: handle user input.
until [[ $running -eq 0 ]]; do
    # Get user input.
    ! read -r -s -n 1 -t 0.01

    # If user has entered something, act on user input.
    if [[ ${PIPESTATUS[0]} -eq 0 ]]; then
        key_pressed "$REPLY"
    fi
done

# Attempt to save todo items.
printf "\033[2J\033[H"
todo_write
